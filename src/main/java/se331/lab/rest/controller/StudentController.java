package se331.lab.rest.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;
import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> student;
    public StudentController(){
        this.student = new ArrayList<>();
        this.student.add(Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("http://13.250.41.39/image/tu.jpg")
                .penAmount(15)
                .description("The great man ever!!!")
                .build());
        this.student.add(Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Cherprang")
                .surname("BNK48")
                .gpa(4.01)
                .image("http://13.250.41.39/image/cherprang.jpg")
                .penAmount(2)
                .description("Code for Thaiand")
                .build());
        this.student.add(Student.builder()
                .id(3l)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("http://13.250.41.39/image/nobita.gif")
                .penAmount(0)
                .description("Welcome to olympic")
                .build());
    }
    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return ResponseEntity.ok(student);
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return ResponseEntity.ok(student.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.student.size());
        this.student.add(student);
        return  ResponseEntity.ok(student);
    }
}
